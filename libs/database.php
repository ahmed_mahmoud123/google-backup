<?php

class Database{

    private static $instance = NULL;
    public $connection;
    public static function getInstance(){
        if(self::$instance == null)
            self::$instance = new Database();
        return self::$instance;
    }

    private function __construct(){
        $this->connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
    }

    public function select($query){
        $result = $this->connection->query($query);
        return $result->fetch_object();
    }


    public function write($query){
        return $this->connection->query($query);
    }

}


?>