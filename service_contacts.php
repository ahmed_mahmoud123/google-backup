<?php
require 'config.php';
$client->setAccessToken((Array)json_decode($_GET['access_token']));
$user_id = $_GET['id'];
$folderName = "backup_contacts_{$user_id}";
exec("mkdir {$folderName}");
$csvFile = $folderName.'/backup_contacts_.csv';
$fp = fopen($csvFile, 'w');


$people_service = new Google_Service_PeopleService($client);
$connections = $people_service->people_connections->listPeopleConnections(
  'people/me', array('personFields' => 'names,emailAddresses,phoneNumbers'));

$con = $connections->getConnections();
foreach($con as $c){
   $names  = $c->getNames();
   $phones = $c->getPhoneNumbers();
   fputcsv($fp,array($names[0]->displayName,$phones[0]->canonicalForm));
}
fclose($fp);
compressFolder($folderName);
exec("rm -r $folderName");
echo json_encode(array("result"=>true));
