<?php
require 'config.php';
$client->setAccessToken((Array)json_decode($_GET['access_token']));

$gmail_service=new Google_Service_Gmail($client);
// Return My Gmail Messages
$messages=$gmail_service->users_messages->listUsersMessages("me",['maxResults'=>'10']);
foreach ($messages as $message) {
  try {
    $gmail_service->users_messages->delete("me",$message->getId());
  } catch (Exception $e) {
  }
}
echo json_encode(array("result"=>true));
?>
