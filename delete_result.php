<?php
require 'header.php';
require 'menu.php';
?>

    <p class="text-center font-weight-light" style="margin-top:4%">
        Your data is successfully deleted.
    </p>


    <div class="row justify-content-center" style="margin-top:3%">
      <div class="card col-md-3 col-sm-6" style="margin:12px">
        <div class="card-body text-center">
            <a href="<?=SITE_URL.'/dashboard.php?id='.$_GET['id']?>">
                <i class="fas fa-check-circle fa-10x"></i>
            </a>
        </div>
      </div>
    </div>


<?php require 'footer.php';?>