<?php
session_start();
ini_set('display_errors',0);
date_default_timezone_set("Africa/Cairo");
require_once __DIR__.'/libs/google_sdk/vendor/autoload.php';
require_once __DIR__.'/libs/database.php';
require_once __DIR__.'/libs/helper.php';


define("SITE_URL","http://35.190.155.16/google-backup/");
//define("SITE_URL","http://127.0.0.1/google-backup/");

define("DB_HOST","localhost");
define("DB_USERNAME","root");
define("DB_PASSWORD","123456");
define("DB_NAME","google_backup");
define("GOOGLE_CLIENT_ID","1090096002566-6n6k1h59ojde2g5167li38odq2qpoj16.apps.googleusercontent.com");
define("GOOGLE_CLIENT_SECRET","a-3b1fXKDEVqqpkPcDeNIWHa");
define("DS",DIRECTORY_SEPARATOR);

$client=new Google_Client();
$client->setClientId(GOOGLE_CLIENT_ID);
$client->setClientSecret(GOOGLE_CLIENT_SECRET);
$client->setRedirectUri("http://35.190.155.16.xip.io/google-backup/callback.php");
//$client->setRedirectUri("http://127.0.0.1/google-backup/callback.php");
$client->addScope("email");
$client->addScope("profile");
$client->addScope(Google_Service_Gmail::GMAIL_SEND);
$client->addScope(Google_Service_Gmail::GMAIL_READONLY);
$client->addScope(Google_Service_Gmail::GMAIL_COMPOSE);
$client->addScope("https://mail.google.com/");
$client->addScope("https://www.googleapis.com/auth/contacts.readonly");
$client->addScope("https://www.google.com/m8/feeds/");
$client->addScope("https://www.googleapis.com/auth/drive");
$client->setAccessType('offline');
?>