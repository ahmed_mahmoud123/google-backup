<?php
require __DIR__.'/header.php';
$url=$client->createAuthUrl();
 ?>


    <div class="row">
        <div class="card col-md-6 col-sm-12 col-md-offset-4" style="margin:5% auto">
            <div class="card-body text-center">
                <h4 class="font-weight-light">
                    Backup your google account today
                </h4>

                
                <a href="<?=$url?>" class="btn btn-primary btn-lg" style="margin:15% auto">
                    <i class="fab fa-google fa-lg"></i>
                    Get Started
                </a>

                <p class="font-weight-light" style="font-size:12px">
                    We use google APIs to access your account <br>
                    We never store your data on our servers permanently <br>
                    Read our TOS and privacy policy
                </p>

            </div>
        </div>    
                
    </div>
 <?php require __DIR__.'/footer.php';?>
