<?php 
    require 'header.php'; 
    require 'menu.php'; 
?>

    <p class="text-center font-weight-light" style="margin-top:4%">
        Welcome <span class="font-weight-bold">
            <?= $loggedUser->name ?></span> ,what do you want to do today ?
    </p>
    <div class="row justify-content-center" style="margin-top:5%">
            <div class="card col-md-3 col-sm-6" style="margin:12px">
                <div class="card-body text-center">
                    <a href="<?=SITE_URL.'gmail.php?id='.$loggedUser->id?>">
                        <i class="fas fa-envelope fa-7x"></i>
                        <h6 class="font-weight-light">
                            Backup Gmail
                        </h6>
                    </a>
                </div>
            </div>

            <div class="card col-md-3 col-sm-6" style="margin:12px">
                <div class="card-body text-center">
                    <a href="<?=SITE_URL.'drive.php?id='.$loggedUser->id?>">
                        <i class="fab fa-google-drive fa-7x"></i>
                        <h6 class="font-weight-light">
                            Backup G-Drive
                        </h6>
                    </a>
                </div>
            </div>

            <div class="card col-md-3 col-sm-6" style="margin:12px">
                <div class="card-body text-center">
                    <a href="<?=SITE_URL.'contacts.php?id='.$loggedUser->id?>">
                        <i class="fab fa-google-plus-g fa-7x"></i>
                        <h6 class="font-weight-light">
                            Backup G-Contacts
                        </h6>
                    </a>
                </div>
            </div>
    </div>


<div class="row justify-content-center">
    <div class="card col-md-3 col-sm-6" style="margin:12px">
        <div class="card-body text-center">
            <a href="<?=SITE_URL.'delete_gmail.php?id='.$loggedUser->id?>">
                <i class="fas fa-envelope fa-7x"></i>
                <h6 class="font-weight-light">
                    Clean Gmail <br>
                    <span class="badge badge-danger">(make backup first)</span>
                </h6>
            </a>
        </div>
    </div>

    <div class="card col-md-3 col-sm-6" style="margin:12px">
        <div class="card-body text-center">
            <a href="">
                <i class="fas fa-envelope fa-7x"></i>
                <h6 class="font-weight-light">
                    Restore Gmail <br>
                    <span class="badge badge-light">(coming soon)</span>
                </h6>
            </a>
        </div>
    </div>

</div>

    
<?php require 'footer.php';?>