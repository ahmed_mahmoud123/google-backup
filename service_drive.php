<?php
require 'config.php';
$client->setAccessToken((Array)json_decode($_GET['access_token']));
$user_id = $_GET['id'];
$backupFolder = "backup_drive_{$user_id}";
exec("mkdir {$backupFolder}");

$service = new Google_Service_Drive($client);
$optParams = array(
  'pageSize' => 5
);
$results = $service->files->listFiles($optParams);
$files = $results->getFiles();
foreach($files as $file){
  checkFile($file->id);
}

compressFolder($backupFolder);
exec("rm -r $backupFolder");
echo json_encode(array("result"=>true));

function checkFile($fileId){
  global $client;
  $httpClient = $client->authorize();
  $request = new GuzzleHttp\Psr7\Request('GET', "https://www.googleapis.com/drive/v2/files/".$fileId);
  $response = $httpClient->send($request);
  $file = json_decode($response->getBody());
  if($file->mimeType != "application/vnd.google-apps.folder"){
      downloadFile($file);
  }
}

function downloadFile($file){
  global $client;
  global $service;
  global $backupFolder;
  $content = null;
  $path = $file->title;
  if(isset($file->webContentLink)){
    $content = file_get_contents($file->webContentLink);
  }else{
    $mineType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    $path.=".docx";
    if($file->mimeType!="application/vnd.google-apps.document"){
      $mineType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      $path.=".xlsx";
    }
    $response = $service->files->export($file->id, $mineType, array('alt' => 'media'));
    $content = $response->getBody()->getContents();
  }
  file_put_contents($backupFolder.'/'.$path,$content);
}

/*var_dump(retrieveAllFiles($service));
function retrieveAllFiles($service) {
  $result = array();
  $pageToken = NULL;

  do {
    try {
      $parameters = array();
      if ($pageToken) {
        $parameters['pageToken'] = $pageToken;
      }
      $files = $service->files->listFiles($parameters);

      $result = array_merge($result, $files->getFiles());
      $pageToken = $files->getNextPageToken();
    } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
      $pageToken = NULL;
    }
  } while ($pageToken);
  return $result;
}*/
/*$backupFolderName = 'backup';

// Print the names and IDs for up to 10 files.
$optParams = array(
  'pageSize' => 5,
  'orderBy'=>'orderBy=folder desc',
  'fields' => 'files(id, name)'
);
$results = $service->files->listFiles($optParams);

if (count($results->getFiles()) == 0) {
    print "No files found.\n";
} else {
    exec("mkdir {$backupFolderName}");
    foreach ($results->getFiles() as $file) {
        checkFile($file->getId(),null);
    }
    compressFolder($backupFolderName);
    header("location:/google-backup/$backupFolderName.zip");
}







function browserFolder($folderId,$path=''){
  global $client;
  $httpClient = $client->authorize();
  echo $path.'<br/>';
  $request = new GuzzleHttp\Psr7\Request('GET', "https://www.googleapis.com/drive/v2/files/{$folderId}/children");
  $response = $httpClient->send($request);
  $response = json_decode($response->getBody());
  foreach($response->items as $item){
    checkFile($item->id,$path);
  }

}

function checkFile($fileId,$parent){
  global $client;
  global $backupFolderName;
  $httpClient = $client->authorize();
  $request = new GuzzleHttp\Psr7\Request('GET', "https://www.googleapis.com/drive/v2/files/".$fileId);
  $response = $httpClient->send($request);
  $file = json_decode($response->getBody());
  if($file->mimeType == "application/vnd.google-apps.folder"){
      if(isset($parent)){
        $parent.="/".$file->title;
      }else{
        $parent = $file->title;
      }
      browserFolder($file->id,$parent);
      //echo "Folder : ".$file->title;
      //echo "<br/>";
  }else{
      exec("mkdir -p {$backupFolderName}/'{$parent}'");
      downloadFile($file,$parent.'/'.$file->title);
  }
}

function downloadFile($file,$path){
  global $client;
  global $service;
  global $backupFolderName;
  $content = null;
  if(isset($file->webContentLink)){
    $content = file_get_contents($file->webContentLink);
  }else{
    $mineType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    $path.=".docx";
    if($file->mimeType!="application/vnd.google-apps.document"){
      $mineType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      $path.=".xlsx";
    }
    $response = $service->files->export($file->id, $mineType, array('alt' => 'media'));
    $content = $response->getBody()->getContents();
  }
  file_put_contents($backupFolderName.'/'.$path,$content);
}


function compressFolder($backupFolderName){
  $rootPath = realpath($backupFolderName);
  $zip = new ZipArchive();
  $zip->open("$backupFolderName.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
  $files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($rootPath),
      RecursiveIteratorIterator::LEAVES_ONLY
  );
  foreach ($files as $name => $file)
  {
      if (!$file->isDir())
      {
          $filePath = $file->getRealPath();
          $relativePath = substr($filePath, strlen($rootPath) + 1);
          $zip->addFile($filePath, $relativePath);
      }
  }
  $zip->close();
}
*/