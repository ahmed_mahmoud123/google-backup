-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 14, 2018 at 01:12 PM
-- Server version: 5.7.13
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `google_backup`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `access_token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `picture`, `email`, `access_token`) VALUES
('103454433860256973294', 'Open Source Web And Mobile Stack', 'https://lh4.googleusercontent.com/-UDFE11SFh3E/AAAAAAAAAAI/AAAAAAAAACE/DkFxaxGnbvA/photo.jpg', 'sw.jokar@gmail.com', '{\"access_token\":\"ya29.Glv4BRqwGpQ1-L92szbMe7KZbDbhH2JS68w6bWLZrK4T0IkmolXRev61izoP9XUOXgTmPZlF3PbKU2ipq6wMZh73keSrQ1crFbC-Gp1Y3s5aM2EfYDWMzt4WFDnp\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"id_token\":\"eyJhbGciOiJSUzI1NiIsImtpZCI6IjhjOWViOTY4ZjczNzQ0ZWFlZDQyMWU0ODAxMDE0MmJjZTUxYTA2N2YifQ.eyJhenAiOiIxMDkwMDk2MDAyNTY2LTZuNmsxaDU5b2pkZTJnNTE2N2xpMzhvZHEycXBvajE2LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTA5MDA5NjAwMjU2Ni02bjZrMWg1OW9qZGUyZzUxNjdsaTM4b2RxMnFwb2oxNi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwMzQ1NDQzMzg2MDI1Njk3MzI5NCIsImVtYWlsIjoic3cuam9rYXJAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiI5Mm1SYWdjWEtuNVdobTE3REVhT0dnIiwiZXhwIjoxNTMxNTcwMDc4LCJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE1MzE1NjY0NzgsIm5hbWUiOiJPcGVuIFNvdXJjZSBXZWIgQW5kIE1vYmlsZSBTdGFjayIsInBpY3R1cmUiOiJodHRwczovL2xoNC5nb29nbGV1c2VyY29udGVudC5jb20vLVVERkUxMVNGaDNFL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUNFL0RrRnhheEduYnZBL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJPcGVuIFNvdXJjZSIsImZhbWlseV9uYW1lIjoiV2ViIEFuZCBNb2JpbGUgU3RhY2siLCJsb2NhbGUiOiJlbiJ9.DkzqB1qTUq65Cms5RPk703fNPMpYUmJACs1pMy4fBjBtc5t3184OKPZJQ0VpFAu-t6b7gXZJ9cXkHVJXKYMM-IxPWOp8Y4UWsBndX_QmbWZSMNUsdi6i2goMnNLDwHEco6DE6oEicf8p_H0Zan9gReBRCHQx8sAR-K9VtbCnriPY43lX8mgzSBgHCnTYmpsvLGIMfYdaGLE7vI-OyQp3xUGLUSmvF1Groee4ErvgNv5kVHNbawG2k7vOrIssJHwUDKKWPUqh7j4BdPSxlYkf5Mpb8ZLkxOLSAoZrsHeDdeIBPqzr7xWJI3vasOPIKgJuMNkJUj0dLj2NdECn2LURTg\",\"created\":1531566478}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
