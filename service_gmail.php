<?php
require 'config.php';
$client->setAccessToken((Array)json_decode($_GET['access_token']));
$user_id = $_GET['id'];
$folderName = "backup_gmail_message_{$user_id}";
exec("mkdir {$folderName}");
$csvFile = $folderName.'/backup_gmail_message_.csv';
$fp = fopen($csvFile, 'w');
$gmail_service=new Google_Service_Gmail($client);
// Return My Gmail Messages
$messages=$gmail_service->users_messages->listUsersMessages("me",['maxResults'=>'10']);
foreach ($messages as $message) {
  //var_dump($message);
  $msg=$gmail_service->users_messages->get("me",$message->getId(),["format"=>"RAW"]);
  //$msg=$gmail_service->users_messages->get("me",$message->getId());
  fputcsv($fp,array($msg->snippet,$msg->raw));
}
fclose($fp);
compressFolder($folderName);
exec("rm -r $folderName");
echo json_encode(array("result"=>true));
?>
