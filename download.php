<?php
require 'header.php';
require 'menu.php';

$zipFileName = '';

if(isset($_GET['op'])){
  $op = $_GET['op'];
  if($op == "gmail"){
    $zipFileName = "backup_gmail_message_{$_GET['id']}.zip";
  }else if($op == "contacts"){
    $zipFileName = "backup_contacts_{$_GET['id']}.zip";
  }else if($op == "drive"){
    $zipFileName = "backup_drive_{$_GET['id']}.zip";
  }
}else{
  header("location:".SITE_URL);
}

if(!file_exists($zipFileName)){
  header("location:".SITE_URL);
}
?>

    <p class="text-center font-weight-light" style="margin-top:4%">
        Your data is successfully gathered. Please download it from the archive below:
    </p>


    <div class="row justify-content-center" style="margin-top:3%">
      <div class="card col-md-3 col-sm-6" style="margin:12px">
        <div class="card-body text-center">
            <a href="<?=SITE_URL.$zipFileName?>">
                <i class="fas fa-file-archive fa-7x"></i>
                <h6 class="font-weight-light" style="font-size:12px">
                    <?= $zipFileName ?>
                </h6>
            </a>
        </div>
      </div>
    </div>

    <h6 class="text-center font-weight-light" style="margin:3% auto;font-size:12px">(this file will be automatically be deleted within 24 hours!)</h6>

<?php require 'footer.php';?>