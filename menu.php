<?php
    if(!isset($_GET['id'])){
        header("location:".SITE_URL);
    }else{
        $id = $_GET['id'];
        $loggedUser = $dbh->select("select * from users where id = '{$id}' ");
    }

?>
<span id="access_token" style="display:none"><?=$loggedUser->access_token?></span>
<span id="user_id" style="display:none"><?=$loggedUser->id?></span>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?=SITE_URL.'/dashboard.php?id='.$_GET['id']?>">
            <i class="fas fa-home"></i>
            Home <span class="sr-only">(current)</span>
        </a>
      </li>
     
    </ul>
    <span class="navbar-text">
      <a class="nav-link" href="<?=SITE_URL.'/logout.php?id='.$_GET['id']?>">
          <i class="fas fa-sign-out-alt"></i>
          Logout
      </a>
    </span>
    <span class="navbar-text">
      <img class="img-thumbnail img-circle img-responsive" width="40" height="40" src="<?= $loggedUser->picture ?>" alt="">
    </span>
  </div>
</nav>