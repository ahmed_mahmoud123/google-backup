<?php
require 'header.php';
require 'menu.php';
?>

    <p class="text-center font-weight-light" style="margin-top:4%">
        Collection data from your Google Drive and archiving it 
    </p>


    <div class="row justify-content-center" style="margin-top:3%">
      <div class="col-md-3 col-sm-6">
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
        </div>
      </div>
    </div>

    <h6 class="text-center font-weight-light" style="margin-top:3%;font-size:12px">
        (this can take a while ,please keep this tab open)
  </h6>

<?php require 'footer.php';?>
<script>

  $(document).ready(function(){
    $.ajax({
      url:"/google-backup/service_drive.php?id="+$("#user_id").text()+"&access_token="+$("#access_token").text(),
      dataType:'json',
      contentType:'application/json',  
      success:function(response){
        if(response.result){
          window.location.replace("/google-backup/download.php?op=drive&id="+$("#user_id").text());
        }
      },
      error:function(){

      }
    })

  });
</script>